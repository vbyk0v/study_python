# -*- coding: utf-8 -*-

from my_logger.my_logger import MyLogger
from my_logger.my_logger import LogLevel


def main():
	MyLogger.set_log_filename("fubar.txt")
	logger = MyLogger()

	for i in range(0, 10):
		logger.log(i**2, LogLevel.DEBUG)


if __name__ == '__main__':
	main()
