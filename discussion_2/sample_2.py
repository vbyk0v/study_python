# -*- coding: utf-8 -*-

def is_palindrom(some_str):
	return some_str == some_str[::-1]


def is_palindrom_good(some_str):
	return some_str.replace(' ','') == some_str[::-1].replace(' ','')

#################################################################

def launch_with_try(fn):
	'''
		Magic here, wrapps any function into try - except individually
	'''
	def wrapper(*args):
		try:
			fn(*args)
		except AssertionError as e:
			print(str(e))
	return wrapper


class MySimpleTestCase:
	__number_of_launches = 0
	__number_of_good_launches = 0

	def __init__(self, some_strange_developer_method):
		self.__target = some_strange_developer_method


	@launch_with_try
	def test_primitive(self):
		MySimpleTestCase.__number_of_launches += 1

		data1 = "racecar"
		data2 = "anna"
		data3 = "annaasdfasfsafasf"

		assert self.__target(data1), "test_primitive 1 FAIL"
		assert self.__target(data2), "test_primitive 2 FAIL"
		assert not self.__target(data3), "test_primitive 3 FAIL"

		MySimpleTestCase.__number_of_good_launches += 1


	@launch_with_try
	def test_fine(self):
		MySimpleTestCase.__number_of_launches += 1

		data = u'а роза упала на лапу азора'
		bad_data = u'а роза 2314 упала н44а л6666апу азора'

		assert self.__target(data), "test_fine 1 FAIL"
		assert not self.__target(bad_data), "test_fine 2 FAIL"

		MySimpleTestCase.__number_of_good_launches += 1


	@classmethod
	def get_success_percentage(cls):
		return 100 * cls.__number_of_good_launches / cls.__number_of_launches



def main():
	try:
		test1 = MySimpleTestCase(is_palindrom)
		test1.test_primitive()
		test1.test_fine()

		test1 = MySimpleTestCase(is_palindrom_good)
		test1.test_primitive()
		test1.test_fine()
	except AssertionError as e:
		print(str(e))
	finally:
		print("OK")
		print("Success percentage: {} %".format(MySimpleTestCase.get_success_percentage()))


if __name__ == "__main__":
	main()
