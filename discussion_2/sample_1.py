# -*- coding: utf-8 -*-

import logging 

__logger = None
LOG_FILE_NAME = "logs.txt"

def create_logger():
	global __logger

	__logger = logging.getLogger('study_logger')
	__logger.setLevel(logging.INFO)

	fh = logging.FileHandler(LOG_FILE_NAME)
	fh.setLevel(logging.INFO)

	formatstr = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
	formatter = logging.Formatter(formatstr)
	fh.setFormatter(formatter)

	__logger.addHandler(fh)


def some_magic_method():
	for i in range(0, 10):
		__logger.info(i ** 3)

	__logger.error("Python without classes is lame")


def main():
	create_logger()
	some_magic_method()


if __name__ == "__main__":
	main()
