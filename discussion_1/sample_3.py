# -*- coding: utf-8 -*-

def some_func():
	print("Hello, World!")

def other_func():
	print("ROFL, LOL!!!111")

def add_nums(a, b):
	return a + b 

def mult_nums(a, b):
	return a * b 

def main():
	list_of_functions = []
	list_of_functions.append(some_func)
	list_of_functions.append(other_func)

	for func in list_of_functions:
		func()

	###############################################################

	my_commands = {
		"do1": some_func,
		"do2": other_func
	}

	command = input("Enter command: ")
	if not command in my_commands:
		print("Wrong command")
	else:
		my_commands[command]()


	###############################################################

	my_other_commands = {
		"add": add_nums,
		"mult": mult_nums
	}

	first = input("Enter first number: ")
	second = input("Enter second number: ")
	command = input("Enter command: ")
	
	if not command in my_other_commands:
		print("Wrong command")
	else:
		print(my_other_commands[command](first, second))


if __name__ == "__main__":
	main()
